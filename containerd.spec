%if 0%{?fedora} || 0%{?rhel} == 6
%global with_devel 1
%global with_bundled 0
%global with_debug 0
%global with_check 1
%global with_unit_test 1
%else
%global with_devel 0
%global with_bundled 0
%global with_debug 0
%global with_check 0
%global with_unit_test 0
%endif

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%global provider        github
%global provider_tld    com
%global project         docker
%global repo            containerd
# https://github.com/docker/containerd
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global commit          2e4e484237cf98ce0c5250b41a6f4ff4fc7a7bd8
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

Name:           golang-%{provider}-%{project}-%{repo}
Version:        0
Release:        0.1.git%{shortcommit}%{?dist}
Summary:        !!!!FILL!!!!
License:        !!!!FILL!!!!
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}

%description
%{summary}

%if 0%{?with_devel}
%package devel
Summary:       %{summary}
BuildArch:     noarch

%if 0%{?with_check} && ! 0%{?with_bundled}
BuildRequires: golang(github.com/Azure/go-ansiterm)
BuildRequires: golang(github.com/Azure/go-ansiterm/winterm)
BuildRequires: golang(github.com/Microsoft/go-winio)
BuildRequires: golang(github.com/Sirupsen/logrus)
BuildRequires: golang(github.com/cloudfoundry/gosigar)
BuildRequires: golang(github.com/coreos/go-systemd/activation)
BuildRequires: golang(github.com/docker/docker/pkg/system)
BuildRequires: golang(github.com/docker/docker/pkg/term/windows)
BuildRequires: golang(github.com/docker/go-connections/sockets)
BuildRequires: golang(github.com/docker/go-units)
BuildRequires: golang(github.com/go-check/check)
BuildRequires: golang(github.com/golang/protobuf/proto)
BuildRequires: golang(github.com/opencontainers/runc/libcontainer/system)
BuildRequires: golang(github.com/opencontainers/runc/libcontainer/user)
BuildRequires: golang(github.com/opencontainers/runtime-spec/specs-go)
BuildRequires: golang(github.com/opencontainers/specs/specs-go)
BuildRequires: golang(github.com/rcrowley/go-metrics)
BuildRequires: golang(github.com/vdemeester/shakers)
BuildRequires: golang(golang.org/x/net/context)
BuildRequires: golang(golang.org/x/net/http2)
BuildRequires: golang(golang.org/x/net/http2/hpack)
BuildRequires: golang(golang.org/x/net/internal/timeseries)
BuildRequires: golang(golang.org/x/net/proxy)
BuildRequires: golang(golang.org/x/net/trace)
BuildRequires: golang(golang.org/x/sys/unix)
BuildRequires: golang(google.golang.org/grpc)
BuildRequires: golang(google.golang.org/grpc/codes)
BuildRequires: golang(google.golang.org/grpc/credentials)
BuildRequires: golang(google.golang.org/grpc/grpclog)
BuildRequires: golang(google.golang.org/grpc/internal)
BuildRequires: golang(google.golang.org/grpc/metadata)
BuildRequires: golang(google.golang.org/grpc/naming)
BuildRequires: golang(google.golang.org/grpc/peer)
BuildRequires: golang(google.golang.org/grpc/transport)
%endif

Requires:      golang(github.com/Azure/go-ansiterm)
Requires:      golang(github.com/Azure/go-ansiterm/winterm)
Requires:      golang(github.com/Microsoft/go-winio)
Requires:      golang(github.com/Sirupsen/logrus)
Requires:      golang(github.com/cloudfoundry/gosigar)
Requires:      golang(github.com/coreos/go-systemd/activation)
Requires:      golang(github.com/docker/docker/pkg/system)
Requires:      golang(github.com/docker/docker/pkg/term/windows)
Requires:      golang(github.com/docker/go-connections/sockets)
Requires:      golang(github.com/docker/go-units)
Requires:      golang(github.com/go-check/check)
Requires:      golang(github.com/golang/protobuf/proto)
Requires:      golang(github.com/opencontainers/runc/libcontainer/system)
Requires:      golang(github.com/opencontainers/runc/libcontainer/user)
Requires:      golang(github.com/opencontainers/runtime-spec/specs-go)
Requires:      golang(github.com/opencontainers/specs/specs-go)
Requires:      golang(github.com/rcrowley/go-metrics)
Requires:      golang(github.com/vdemeester/shakers)
Requires:      golang(golang.org/x/net/context)
Requires:      golang(golang.org/x/net/http2)
Requires:      golang(golang.org/x/net/http2/hpack)
Requires:      golang(golang.org/x/net/internal/timeseries)
Requires:      golang(golang.org/x/net/proxy)
Requires:      golang(golang.org/x/net/trace)
Requires:      golang(golang.org/x/sys/unix)
Requires:      golang(google.golang.org/grpc)
Requires:      golang(google.golang.org/grpc/codes)
Requires:      golang(google.golang.org/grpc/credentials)
Requires:      golang(google.golang.org/grpc/grpclog)
Requires:      golang(google.golang.org/grpc/internal)
Requires:      golang(google.golang.org/grpc/metadata)
Requires:      golang(google.golang.org/grpc/naming)
Requires:      golang(google.golang.org/grpc/peer)
Requires:      golang(google.golang.org/grpc/transport)

Provides:      golang(%{import_path}) = %{version}-%{release}
Provides:      golang(%{import_path}/api/grpc/server) = %{version}-%{release}
Provides:      golang(%{import_path}/api/grpc/types) = %{version}-%{release}
Provides:      golang(%{import_path}/api/http/pprof) = %{version}-%{release}
Provides:      golang(%{import_path}/archutils) = %{version}-%{release}
Provides:      golang(%{import_path}/osutils) = %{version}-%{release}
Provides:      golang(%{import_path}/runtime) = %{version}-%{release}
Provides:      golang(%{import_path}/specs) = %{version}-%{release}
Provides:      golang(%{import_path}/supervisor) = %{version}-%{release}
Provides:      golang(%{import_path}/testutils) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/Azure/go-ansiterm) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/Azure/go-ansiterm/winterm) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/Microsoft/go-winio) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/Sirupsen/logrus) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/cloudfoundry/gosigar) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/codegangsta/cli) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/coreos/go-systemd/activation) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/cyberdelia/go-metrics-graphite) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/docker/pkg/integration/checker) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/docker/pkg/listeners) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/docker/pkg/system) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/docker/pkg/term) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/docker/pkg/term/windows) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/docker/go-connections/sockets) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/go-check/check) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/golang/protobuf/proto) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/opencontainers/runc/libcontainer/system) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/opencontainers/runc/libcontainer/user) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/opencontainers/runtime-spec/specs-go) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/rcrowley/go-metrics) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/github.com/vdemeester/shakers) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/net/context) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/net/http2) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/net/http2/hpack) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/net/proxy) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/net/trace) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/golang.org/x/sys/unix) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/codes) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/credentials) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/grpclog) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/metadata) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/naming) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/peer) = %{version}-%{release}
Provides:      golang(%{import_path}/vendor/src/google.golang.org/grpc/transport) = %{version}-%{release}

%description devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package unit-test-devel
Summary:         Unit tests for %{name} package
%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        %{name}-devel = %{version}-%{release}

%description unit-test-devel
%{summary}

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif

%prep
%setup -q -n %{repo}-%{commit}

%build

%install
# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . -iname "*.go" \! -iname "*_test.go") ; do
    echo "%%dir %%{gopath}/src/%%{import_path}/$(dirname $file)" >> devel.file-list
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test.file-list
for file in $(find . -iname "*_test.go"); do
    echo "%%dir %%{gopath}/src/%%{import_path}/$(dirname $file)" >> devel.file-list
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else
export GOPATH=%{buildroot}/%{gopath}:$(pwd)/Godeps/_workspace:%{gopath}
%endif

%gotest %{import_path}/integration-test
%gotest %{import_path}/runtime
%gotest %{import_path}/supervisor
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%if 0%{?with_devel}
%files devel -f devel.file-list
%license 
%doc CONTRIBUTING.md README.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files unit-test-devel -f unit-test-devel.file-list
%license 
%doc CONTRIBUTING.md README.md
%endif

%changelog
* Thu Jun 30 2016 Tim Orling - 0-0.1.git2e4e484
- First package for Fedora


